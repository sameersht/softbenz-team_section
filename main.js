/** ==========================================================================================

  * Name: Softbenz Infosys
  * Version: 2.0 (Date: 30 March 2021)
  * Author: Softbenz / Arjun Saud
  * Author URI: https://www.softbenz.com/

========================================================================================== */

!(function (t) {
  "use strict";
  t(window).on("load", function () {
    t("#preloader").fadeOut(), t("#status").fadeOut(3e3);
  }),
    jQuery(".ttm-header-search-link a").addClass("sclose"),
    jQuery(".ttm-header-search-link a").on("click", function (t) {
      jQuery(".field.searchform-s").focus(),
        jQuery(".ttm-header-search-link a").hasClass("sclose")
          ? (jQuery(".ttm-header-search-link a i")
              .removeClass("ti-search")
              .addClass("ti-close"),
            jQuery(this).removeClass("sclose").addClass("open"),
            jQuery(".ttm-search-overlay").addClass("st-show"))
          : (jQuery(this).removeClass("open").addClass("sclose"),
            jQuery(".ttm-header-search-link a i")
              .removeClass("ti-close")
              .addClass("ti-search"),
            jQuery(".ttm-search-overlay").removeClass("st-show")),
        t.preventDefault();
    }),
    t(window).scroll(function () {
      matchMedia("only screen and (min-width: 1200px)").matches &&
        (t(window).scrollTop() >= 50
          ? (t(".ttm-stickable-header").addClass("fixed-header"),
            t(".ttm-stickable-header").addClass("visible-title"))
          : (t(".ttm-stickable-header").removeClass("fixed-header"),
            t(".ttm-stickable-header").removeClass("visible-title")));
    }),
    t("ul li:has(ul)").addClass("has-submenu"),
    t("ul li ul").addClass("sub-menu"),
    t("ul.dropdown li").on({
      mouseover: function () {
        t(this).addClass("hover");
      },
      mouseout: function () {
        t(this).removeClass("hover");
      },
    }),
    t("ul.dropdown li").click(function () {
      t("div").removeClass("hover");
    });
  var e = t("#menu"),
    a = t("#menu-toggle-form"),
    s = t(".has-submenu > a");
  a.on("click", function (t) {
    a.toggleClass("active"), e.toggleClass("active");
  }),
    s.on("click", function (e) {
      e.preventDefault(),
        t(this).toggleClass("active").next("ul").toggleClass("active");
    }),
    t("ul li:has(ul)"),
    t("[data-appear-animation]").each(function () {
      var e = t(this),
        a = e.data("appear-animation");
      e.data("appear-animation-delay") && e.data("appear-animation-delay");
      t(window).width() > 959
        ? (e.html("0"),
          e.waypoint(
            function (t) {
              if (!e.hasClass("completed")) {
                var a = e.data("from"),
                  s = e.data("to"),
                  i = e.data("interval");
                e.numinate({
                  format: "%counter%",
                  from: a,
                  to: s,
                  runningInterval: 2e3,
                  stepUnit: i,
                  onComplete: function (t) {
                    e.addClass("completed");
                  },
                });
              }
            },
            { offset: "85%" }
          ))
        : "animateWidth" == a && e.css("width", e.data("width"));
    }),
    t(".ttm-progress-bar").each(function () {
      t(this).find(".progress-bar").width(0);
    }),
    t(".ttm-progress-bar").each(function () {
      t(this)
        .find(".progress-bar")
        .animate({ width: t(this).attr("data-percent") }, 2e3);
    }),
    t(".progress-bar-percent[data-percentage]").each(function () {
      var e = t(this),
        a = Math.ceil(t(this).attr("data-percentage"));
      t({ countNum: 0 }).animate(
        { countNum: a },
        {
          duration: 2e3,
          easing: "linear",
          step: function () {
            var t = "";
            (t =
              0 == a
                ? Math.floor(this.countNum) + "%"
                : Math.floor(this.countNum + 1) + "%"),
              e.text(t);
          },
        }
      );
    }),
    t(".ttm-tabs").each(function () {
      t(this).children(".content-tab").children().hide(),
        t(this).children(".content-tab").children().first().show(),
        t(this)
          .find(".tabs")
          .children("li")
          .on("click", function (e) {
            var a = t(this).index(),
              s = t(this)
                .siblings()
                .removeClass("active")
                .parents(".ttm-tabs")
                .children(".content-tab")
                .children()
                .eq(a);
            s.addClass("active").fadeIn("slow"),
              s.siblings().removeClass("active"),
              t(this)
                .addClass("active")
                .parents(".ttm-tabs")
                .children(".content-tab")
                .children()
                .eq(a)
                .siblings()
                .hide(),
              e.preventDefault();
          });
    }),
    t(".accordion").each(function () {
      var e = t(".toggle").children(".toggle-content").hide();
      t(".toggle").children(".toggle-content").eq(1).slideDown("easeOutExpo"),
        t(".toggle")
          .children(".toggle-title")
          .children("a")
          .eq(1)
          .addClass("active"),
        t(".toggle")
          .children(".toggle-title")
          .children("a")
          .on("click", function () {
            var a = t(this).parent().next(".toggle-content");
            return (
              t(".toggle-title > a").removeClass("active"),
              t(this).addClass("active"),
              e.not(a).slideUp("easeInExpo"),
              t(this).parent().next().slideDown("easeOutExpo"),
              !1
            );
          });
    }),
    t(window).on("load", function () {
      var e = t("#isotopeContainer");
      t("#filters a").on("click", function () {
        var a = t(this).attr("data-filter");
        return e.isotope({ filter: a }), !1;
      }),
        t("#filters li")
          .find("a")
          .on("click", function () {
            var a = t(this);
            if (a.hasClass("selected")) return !1;
            var s = a.parents("#filters");
            s.find(".selected").removeClass("selected"), a.addClass("selected");
            var i = {},
              o = s.attr("data-option-key"),
              l = a.attr("data-option-value");
            return (
              (l = "false" !== l && l),
              (i[o] = l),
              "layoutMode" === o && "function" == typeof changeLayoutMode
                ? changeLayoutMode(a, i)
                : e.isotope(i),
              !1
            );
          });
    }),
    jQuery(document).ready(function () {
      jQuery(
        'a[href*=".jpg"], a[href*=".jpeg"], a[href*=".png"], a[href*=".gif"]'
      ).each(function () {
        if (
          "_blank" != jQuery(this).attr("target") &&
          !jQuery(this).hasClass("prettyphoto") &&
          !jQuery(this).hasClass("modula-lightbox")
        ) {
          var e = t(this).attr("data-gal");
          void 0 !== e &&
            !1 !== e &&
            "prettyPhoto" != e &&
            jQuery(this).attr("data-rel", "prettyPhoto");
        }
      }),
        jQuery('a[data-gal^="prettyPhoto"]').prettyPhoto(),
        jQuery("a.ttm_prettyphoto").prettyPhoto(),
        jQuery('a[data-gal^="prettyPhoto"]').prettyPhoto(),
        jQuery("a[data-gal^='prettyPhoto']").prettyPhoto({ hook: "data-gal" });
    }),
    t("#home-main-slider").carousel({
      margin: 0,
      loop: !0,
      nav: t("#home-main-slider").data("nav"),
      dots: t("#home-main-slider").data("dots"),
      autoplay: t("#home-main-slider").data("auto"),
      smartSpeed: 2e3,
      responsive: {
        0: { items: 1 },
        480: { items: 1 },
        991: { items: 1 },
        1000: { items: t("#home-main-slider").data("item") },
      },
    }),
    t(".history-slide").owlCarousel({
      loop: !0,
      margin: 0,
      nav: t(".history-slide").data("nav"),
      dots: t(".history-slide").data("dots"),
      autoplay: t(".history-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        576: { items: 2 },
        768: { items: 2 },
        992: { items: t(".history-slide").data("item") },
      },
    }),
    t(".team-slide").owlCarousel({
      loop: !0,
      margin: 0,
      nav: t(".team-slide").data("nav"),
      dots: t(".team-slide").data("dots"),
      autoplay: t(".team-slide").data("auto"),
      smartSpeed: 1e3,
      responsive: {
        0: { items: 1 },
        480: { items: 2 },
        768: { items: 3 },
        1200: { items: t(".team-slide").data("item") },
      },
    }),
    t(".testimonial-slide").owlCarousel({
      loop: !0,
      margin: 0,
      smartSpeed: 1e3,
      nav: t(".testimonial-slide").data("nav"),
      dots: t(".testimonial-slide").data("dots"),
      autoplay: t(".testimonial-slide").data("auto"),
      responsive: {
        0: { items: 1 },
        600: { items: 1 },
        1000: { items: t(".testimonial-slide").data("item") },
      },
    }),
    t(".portfolio-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".portfolio-slide").data("nav"),
      dots: t(".portfolio-slide").data("dots"),
      autoplay: t(".portfolio-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        576: { items: 1 },
        768: { items: 1 },
        992: { items: 1 },
        1024: { items: t(".portfolio-slide").data("item") },
      },
    }),
    t(".blog-slide").owlCarousel({
      loop: !0,
      margin: 0,
      nav: t(".blog-slide").data("nav"),
      dots: t(".blog-slide").data("dots"),
      autoplay: t(".blog-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        576: { items: 2 },
        992: { items: t(".blog-slide").data("item") },
      },
    }),
    t(".clients-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".clients-slide").data("nav"),
      dots: t(".clients-slide").data("dots"),
      autoplay: t(".clients-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        576: { items: 3 },
        768: { items: 4 },
        992: { items: t(".clients-slide").data("item") },
      },
    }),
    t(".technologies-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".technologies-slide").data("nav"),
      dots: t(".technologies-slide").data("dots"),
      autoplay: t(".technologies-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 2 },
        576: { items: 3 },
        768: { items: 4 },
        992: { items: t(".technologies-slide").data("item") },
      },
    }),
    t(".services-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".services-slide").data("nav"),
      dots: t(".services-slide").data("dots"),
      autoplay: t(".services-slide").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        680: { items: 2 },
        992: { items: t(".services-slide").data("item") },
      },
    }),
    t(".testimonial-slide2").owlCarousel({
      loop: !0,
      margin: 0,
      smartSpeed: 1e3,
      nav: t(".testimonial-slide2").data("nav"),
      dots: t(".testimonial-slide2").data("dots"),
      autoplay: t(".testimonial-slide2").data("auto"),
      responsive: {
        0: { items: 1 },
        576: { items: 2 },
        1000: { items: t(".testimonial-slide2").data("item") },
      },
    }),
    t(".portfolio-img-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".portfolio-img-slide").data("nav"),
      dots: t(".portfolio-img-slide").data("dots"),
      autoplay: t(".portfolio-img-slide").data("auto"),
      smartSpeed: 1e3,
      responsive: {
        0: { items: 1 },
        480: { items: 1 },
        991: { items: 1 },
        1000: { items: t(".portfolio-img-slide").data("item") },
      },
    }),
    t(".project-slide").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".project-slide").data("nav"),
      dots: t(".project-slide").data("dots"),
      autoplay: t(".project-slide").data("auto"),
      smartSpeed: 2e3,
      responsive: {
        0: { items: 1 },
        480: { items: 1 },
        991: { items: 1 },
        1000: { items: t(".project-slide").data("item") },
      },
    }),
    t(".portfolio-slide2").owlCarousel({
      margin: 0,
      loop: !0,
      nav: t(".portfolio-slide2").data("nav"),
      dots: t(".portfolio-slide2").data("dots"),
      autoplay: t(".portfolio-slide2").data("auto"),
      smartSpeed: 3e3,
      responsive: {
        0: { items: 1 },
        576: { items: 2 },
        768: { items: 3 },
        992: { items: t(".portfolio-slide2").data("item") },
      },
    }),
    jQuery(document).ready(function (t) {
      if (jQuery("body").hasClass("ttm-one-page-site")) {
        var e = jQuery(".ttm-row"),
          a = jQuery(".ttm-header-wrap, .menu"),
          s = jQuery("#site-navigation").data("sticky-height") - 1;
        jQuery(window).on("scroll", function () {
          jQuery("body").scrollTop() < 5 &&
            a.find("a").parent().removeClass("active");
          var t = jQuery(this).scrollTop();
          e.each(function () {
            var e = jQuery(this).offset().top - (s + 1),
              i = e + jQuery(this).outerHeight();
            if (
              t >= e &&
              t <= i &&
              void 0 !== jQuery(this) &&
              void 0 !== jQuery(this).attr("id") &&
              "" != jQuery(this).attr("id")
            ) {
              var o = jQuery(this);
              a.find("a").removeClass("active"),
                jQuery(this).addClass("active");
              var l = o.attr("id");
              a.find("a").parent().removeClass("active"),
                a.find("a").each(function () {
                  jQuery(this).attr("href").split("#")[1] == l &&
                    jQuery(this).parent().addClass("active");
                });
            }
          });
        }),
          a.find("a").on("click", function () {
            var t = jQuery(this).attr("href").split("#")[1];
            return (
              jQuery("html, body").animate(
                { scrollTop: jQuery("#" + t).offset().top - s },
                500
              ),
              !1
            );
          });
      }
    }),
    jQuery("#totop").hide(),
    jQuery(window).scroll(function () {
      jQuery(this).scrollTop() >= 100
        ? (jQuery("#totop").fadeIn(200),
          jQuery("#totop").addClass("top-visible"))
        : (jQuery("#totop").fadeOut(200),
          jQuery("#totop").removeClass("top-visible"));
    }),
    jQuery("#totop").on("click", function () {
      return jQuery("body,html").animate({ scrollTop: 0 }, 500), !1;
    }),
    t(function () {}),
    t(document).ready(function () {
      t(".filter-button").click(function () {
        var e = t(this).attr("data-filter");
        "all" == e
          ? t(".filter").show()
          : (t(".filter")
              .not("." + e)
              .hide(),
            t(".filter")
              .filter("." + e)
              .show()),
          t(".filter-button").removeClass("active") &&
            t(this).addClass("active");
      }),
        t(this).addClass("active");
    }),
    t(".portfolio-content-box a").click(function () {
      var e = t(this).attr("href");
      return (
        t(".portfolio-content-box").addClass("item_open"),
        t(e).addClass("item_open"),
        !1
      );
    }),
    t(".close-btn").click(function () {
      return t(".port, .portfolio-content-box").removeClass("item_open"), !1;
    }),
    t(".portfolio-content-box a").click(function () {
      t("html, body").animate(
        { scrollTop: parseInt(t("#top").offset().top) },
        400
      );
    });
})(jQuery);
